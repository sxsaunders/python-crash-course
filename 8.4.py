# -*- coding: utf-8 -*-
"""
Created on Thu Sep 19 20:39:36 2019

@author: snn181
"""

############   8.4
def make_shirt(size = 'large', text = 'I love Python'):
        print(f"The size of your shirt is {size} and '{text}' will be printed on it.")
        
make_shirt()
make_shirt(size = 'medium')
make_shirt(text = 'This is redundant')