# -*- coding: utf-8 -*-
"""
Created on Thu Sep 19 20:39:36 2019

@author: snn181
"""

#############    8.5
def describe_city(city, country = 'USA'):
    print(f'{city} is in {country}')

describe_city('Denver')
describe_city('Cairo', 'Egypt')