# -*- coding: utf-8 -*-
"""
Created on Thu Sep 19 20:39:36 2019

@author: snn181
"""

##############   8.3
def make_shirt(size, text):
    print(f"The size of your shirt is {size} and {text} will be printed on it.")
    
make_shirt('large', 'Shawna rocks!!')
make_shirt(size = 'large', text = 'Shawna rocks!!')